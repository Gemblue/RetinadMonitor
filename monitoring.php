<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		
		<h2>Retinad Wifi Monitoring</h2>
		
		<table class="table table-striped">
			<tr><th>Venue</th><th>Status</th></tr>
			
			<?php
			$handle = fopen("venues.txt", "r");
			if ($handle) 
			{
				while (($line = fgets($handle)) !== false) 
				{
					$line = explode('>', $line);

			    	// On or off
					$host = preg_replace('/\s+/', '', $line[1]);

					// Ping
					exec("ping -n 2 " . $host, $output, $result);
					
					if (!empty($result == 0))
						$status = '<span class="label label-success">On</span>';
					else 
						$status = '<span class="label label-danger">Off</span>';
					
					?>
					
					<tr>
						<td><?php echo $line[0];?></td>
						<td><?php echo $status;?></td>
					</tr>

					<?php

					$content = '';
				}

				fclose($handle);
			}
			else 
			{
				exit('Error opening file.');
			} 
			?>
		</table>

	</div>
</body>
</html>